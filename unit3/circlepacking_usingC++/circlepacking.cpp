#include<iostream>
#include<iomanip>
#include<vector>
using namespace std;

struct Point
{
	float x;
	float y;
};

struct Circle
{
	Point point;
	float radius;
};

inline float GetRandomFloatNumber(float min, float max)
{
	return ((float)rand() / RAND_MAX)*(max - min) + min;
}

vector<Circle> g_circles;

bool CheckIsDistanceVaild(Circle& circle1, Circle& circle2)
{
	float distance_2 = (circle1.point.x - circle2.point.x) *
		(circle1.point.x - circle2.point.x) +
		(circle1.point.y - circle2.point.y) *
		(circle1.point.y - circle2.point.y);
	float minDistance_2 = (circle1.radius + circle2.radius) *
		(circle1.radius + circle2.radius);

	if (distance_2 > minDistance_2)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool CheckIsValid(vector<Circle>& circles, Circle& newCircle)
{
	for (auto circle : circles)
	{
		if (!CheckIsDistanceVaild(circle, newCircle))
		{
			return false;
		}
	}
	return true;
}

void PrintResult(vector<Circle>& circles)
{
	float r2 = 0;
	for (auto circle : circles)
	{
		cout << "x:" << setiosflags(ios::left) << setw(10) << circle.point.x
			<< "y:" << setiosflags(ios::left) << setw(10) << circle.point.y
			<< "r:" << setiosflags(ios::left) << setw(10) << circle.radius
			<< endl;
			r2 = r2+circle.radius*circle.radius;
			
	}
	cout << "sum of square of radius:" << setiosflags(ios::left) << setw(10) << r2 << endl;
}

int main()
{
	const int areaLength = 2;
	const int totalCircleNum = 3;
	const int maxRadius = 1 ;

	unsigned int currentCircleNum = 0;
	while (currentCircleNum < totalCircleNum)
	{
		Circle newCircle = Circle{ GetRandomFloatNumber(-areaLength / 2, areaLength / 2),
					  GetRandomFloatNumber(-areaLength / 2, areaLength / 2),
					  GetRandomFloatNumber(areaLength/ totalCircleNum, maxRadius) };
		if (CheckIsValid(g_circles, newCircle))
		{
			g_circles.push_back(newCircle);
			currentCircleNum++;
		}
	}

	PrintResult(g_circles);
	
	return 0;
	system("pause");
}