import numpy as np
import random
import matplotlib.pyplot as plt
from scipy.optimize import minimize

#定义圆类
class circle:
    def __init__(self, radius = 0, x = 0, y = 0):#构造函数
        self.radius = radius
        self.x = x
        self.y = y
        
    def print_circle(self):
        print('radius={}, coordinate=({},{})'.format(self.radius, self.x, self.y))
    
    #两圆心距离
    def distance(self, c2):       
        dis = ((self.x-c2.x)**2+(self.y-c2.y)**2)**0.5
        return dis
    
    #判断新建的圆是否与原来的重叠
    def overlap(self, c_list):
        for i in range (len(c_list)):
            c2 = c_list[i]
            r1 = self.radius
            r2 = c2.radius
            rr = r1+r2
            dis = self.distance(c2)
            if dis < rr:
                return False
        return True
        
    #判断圆是否超出限定范围 
    def ifexcess(self):
        r = self.radius
        x = self.x
        y = self.y         
        left = abs(x - r)
        right = abs(x + r)
        upper = abs(y + r)
        lower = abs(y - r)
        if max(left, right, upper, lower) > 1:
            return False
        else:
            return True


#找出剩余空间中可行的最大半径
def MaxR(c1, c_list):
    x = c1.x
    y = c1.y
    R_list = [1-x,1+x,1-y,1+y]
    for i in range (len(c_list)):
        c2 = c_list[i]
        dis = c1.distance(c2)
        R_list.append(dis-c2.radius)
    return min(R_list)

#需要优化的目标函数        
def func(c_list):
    return lambda x : 1 - MaxR(circle(x[0], x[1], x[2]), c_list)

#找出最优圆心
def opt_center(c, c_list):
    r = c.radius
    x = c.x
    y = c.y
    rxy = [r,x,y]
    bd_r = (0, 1)
    bd_x = (-1, 1)
    bd_y = (-1, 1)
    bds = (bd_r, bd_x, bd_y)       
    res = minimize(func(c_list), rxy, method='SLSQP', bounds=bds)
    c.x = res.x[1]
    c.y = res.x[2]
    c.radius = MaxR(c, c_list)
    return c

#找m个圆，获得区域内局部最大半径
def FindMaxCircuit(m):
    c_list = []
    for i in range (m):
        r = 0
        x = random.uniform(-1, 1)
        y = random.uniform(-1, 1)
        c = circle(r, x, y)
        while not c.overlap(c_list):           
            x = random.uniform(-1, 1)
            y = random.uniform(-1, 1)
            c = circle(r, x, y)
        c = opt_center(c, c_list)
        c_list.append(c)
    return c_list

def plot(c_list):
    plt.figure()
    plt.axes().set_aspect('equal')
    plt.xlim([-1,1])
    plt.ylim([-1,1])  
    theta = np.linspace(0,2*np.pi,50)
    for c in c_list:
        plt.plot(c.x+c.radius*np.cos(theta),c.y+c.radius*np.sin(theta),'b')       
    plt.show()
    
if __name__ == "__main__":
    m = 3
    c_list = FindMaxCircuit(m)   
    RR = 0
    for c in c_list:
        RR += c.radius**2
        c.print_circle()
    print('for {} circles, the maximize sum of r^2 = {}'.format(m, RR))
    
    plot(c_list)
